/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50505
Source Host           : 127.0.0.1:3306
Source Database       : congthucbanh

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2017-05-14 21:32:58
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for congthucbanh
-- ----------------------------
DROP TABLE IF EXISTS `congthucbanh`;
CREATE TABLE `congthucbanh` (
  `mabanh` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `madanhmuc` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tenbanh` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nguyenlieu` text COLLATE utf8_unicode_ci,
  `noidung` text COLLATE utf8_unicode_ci,
  `hinhanh` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`mabanh`),
  KEY `fk_congthucbanh_danhmuc` (`madanhmuc`),
  CONSTRAINT `fk_congthucbanh_danhmuc` FOREIGN KEY (`madanhmuc`) REFERENCES `danhmucbanh` (`madanhmuc`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of congthucbanh
-- ----------------------------

-- ----------------------------
-- Table structure for danhgia
-- ----------------------------
DROP TABLE IF EXISTS `danhgia`;
CREATE TABLE `danhgia` (
  `mabanh` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `makh` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `diemdanhgia` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`mabanh`),
  CONSTRAINT `fk_danhgia_congthucbanh` FOREIGN KEY (`mabanh`) REFERENCES `congthucbanh` (`mabanh`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of danhgia
-- ----------------------------

-- ----------------------------
-- Table structure for danhmucbanh
-- ----------------------------
DROP TABLE IF EXISTS `danhmucbanh`;
CREATE TABLE `danhmucbanh` (
  `madanhmuc` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `tendanhmuc` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`madanhmuc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of danhmucbanh
-- ----------------------------

-- ----------------------------
-- Table structure for thanhvien
-- ----------------------------
DROP TABLE IF EXISTS `thanhvien`;
CREATE TABLE `thanhvien` (
  `mathanhvien` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `tendangnhap` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `matkhau` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ngaysinh` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gioitinh` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`mathanhvien`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of thanhvien
-- ----------------------------

-- ----------------------------
-- Table structure for video
-- ----------------------------
DROP TABLE IF EXISTS `video`;
CREATE TABLE `video` (
  `mabanh` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `luuhienthi` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `soluotxem` int(11) DEFAULT NULL,
  PRIMARY KEY (`mabanh`),
  CONSTRAINT `fk_video_congthucbanh` FOREIGN KEY (`mabanh`) REFERENCES `congthucbanh` (`mabanh`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of video
-- ----------------------------
