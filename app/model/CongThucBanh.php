<?php

class CongThucBanh {

    private $maBanh;
    private $maDanhMuc;
    private $tenBanh;
    private $nguyenLieu;
    private $noiDung;
    private $hinhAnh;

    public function __construct($maBanh, $maDanhMuc, $tenBanh, $nguyenLieu, $noiDung, $hinhAnh) {
        $this->maBanh = $maBanh;
        $this->maDanhMuc = $maDanhMuc;
        $this->tenBanh = $tenBanh;
        $this->nguyenLieu = $nguyenLieu;
        $this->noiDung = $noiDung;
        $this->hinhAnh = $hinhAnh;
    }

    public function getMaBanh() {
        return $this->maBanh;
    }

    public function setMaBanh($maBanh) {
        $this->maBanh = $maBanh;
    }

    public function getMaDanhMuc() {
        return $this->maDanhMuc;
    }

    public function setMaDanhMuc($maDanhMuc) {
        $this->maDanhMuc = $maDanhMuc;
    }

    public function getTenBanh() {
        return $this->tenBanh;
    }

    public function setTenBanh($tenBanh) {
        $this->tenBanh = $tenBanh;
    }

    public function getNguyenLieu() {
        return $this->nguyenLieu;
    }

    public function setNguyenLieu($nguyenLieu) {
        $this->nguyenLieu = $nguyenLieu;
    }

    public function getNoiDung() {
        return $this->noiDung;
    }

    public function setNoiDung($noiDung) {
        $this->noiDung = $noiDung;
    }

    public function getHinhAnh() {
        return 'public/img/' . $this->hinhAnh;
    }

    public function setHinhAnh($hinhAnh) {
        $this->hinhAnh = $hinhAnh;
    }

}

?>