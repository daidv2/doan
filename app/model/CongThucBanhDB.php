<?php

use Database as DB;

class CongThucBanhDB {
    
    public function response($data){
        return new CongThucBanh(
                $data['mabanh'],
                $data['madanhmuc'],
                $data['tenbanh'],
                $data['nguyenlieu'],
                $data['noidung'],
                $data['hinhanh']);
    }

    public static function getListCongThucBanh() {
        $db = Database::getDB();
        $query = 'SELECT * FROM congthucbanh';
        $statement = $db->prepare($query);
        $statement->execute();
        $listCongThucBanh = $statement->fetchAll(PDO::FETCH_ASSOC);
        $data = array();
        foreach ($listCongThucBanh as $item) {
            $data[] = self::response($item);
        }
        return $data;
    }

    public static function getCongThucBanh($maBanh) {
        $db = Database::getDB();
        $query = 'SELECT * FROM congthucbanh WHERE mabanh = :mabanh';
        $statement = $db->prepare($query);
        $statement->bindValue(':mabanh', $maBanh);
        $statement->execute();
        $congThucBanh = $statement->fetch(PDO::FETCH_ASSOC);
        return self::response($congThucBanh);
    }

}

?>