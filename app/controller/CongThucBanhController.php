<?php


class CongThucBanhController {
    
    public function index() {
        $data = CongThucBanhDB::getListCongThucBanh();
        require 'app/view/index.php';
    }
    
    public function getCongThucBanh(){
        $maBanh = $_GET['maBanh'];
        $data = CongThucBanhDB::getCongThucBanh($maBanh);
        require 'app/view/chitietsanpham.php';
    }
}
?>