<?php

/**
 * Description of message
 *
 * @author DaiDV
 */
class Message {
    
    //common
    const INVALID_METHOD = 'Invalid Method';
    const INVALID_DATA = 'Invalid Data';
    const UNABLE_TO_RETRIEVE_DATA = 'Không lấy được thông tin.';
    const UPDATE_DATA_SUCCESS = 'Cập nhật dữ liệu thành công.';
    const UPDATE_DATA_FAIL = 'Cập nhật dữ liệu thất bại.';
    //cong thuc banh
    const CAKE_INSERT_SUCCESS = 'Thêm công thức bánh thành công.';
    const CAKE_INSERT_FAIL = 'Thêm công thức bánh thất bại.';
    const CAKE_DELETE_SUCCESS = 'Xóa công thức bánh thành công.';
    const CAKE_DELETE_FAIL = 'Xóa công thức bánh thất bại.';
    //danh muc banh
    const CATEGORY_INSERT_SUCCESS = 'Thêm danh mục bánh thành công.';
    const CATEGORY_INSERT_FAIL = 'Thêm danh mục bánh thất bại';
    //thanh vien
    const USER_INSERT_SUCCESS = 'Thêm thành viên thành công.';
    const USER_INSERT_FAIL = 'Thêm thành viên thất bại';
}