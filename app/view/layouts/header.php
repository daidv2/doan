<!doctype html>
<html>
    <head>
        <title>Hướng dẫn làm bánh</title>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="public/css/style.css">
        <style>
        * {
            margin:0px;
            padding:0px;
        }
        .clearfix:after {
            display:block;
            clear:both;
        }
        /*----- Phần menu -----*/
        .banner{
            padding-left: 84px;
        }
        .menu  {
            width:1200px;
            margin:0px auto;
            background:#ce3671;
            height: 50px;

        }
        .menu li {
            margin:0px;
            list-style:none;
            font-family:'Ek Mukta';
        }
        .menu a {
            transition:all linear 0.15s;
            color:#919191;
            text-decoration:none;
        }
        .menu li:hover > a, .menu .current-item > a {
            text-decoration:none;
            color:#be5b70;
        }
        .menu .arrow {
            font-size:11px;
            line-height:0%;
        }
        /*----- css cho phần menu cha -----*/
        .menu > ul > li {
            float:left;
            display:inline-block;
            position:relative;
            font-size:19px;
        }
        .menu > ul > li > a {
            padding:14px 60px;
            display:inline-block;
            color:white;
        }

        .menu > ul > li:hover > a {
            background:#2e2728;
        }
        /*----css cho menu con----*/
        .menu li:hover .sub-menu {
            z-index:1;
            opacity:1;
        }
        .sub-menu {
            width:100%;
            padding:5px 0px;
            position:absolute;
            top:100%;
            left:0px;
            z-index:-1;
            opacity:0;
            transition:opacity linear 0.15s;
            box-shadow:0px 2px 3px rgba(0,0,0,0.2);
            background:#2e2728;
        }
        .sub-menu li {
            display:block;
            font-size:16px;
        }
        .sub-menu li a {
            padding:10px 30px;
            display:block;
        }
    </style>
        <script src="public/js/lib/jquery-1.12.1.min.js" type="text/javascript"></script>
        <script src="public/js/common.js" type="text/javascript"></script>
    </head>    
    <body>
        <div class="banner">
            <img src="public/img/banner.png">
        </div>
        <div class="wrapper">
            <nav class="menu">
                <ul class="clearfix">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Công thức làm bánh</a>
                        <ul class="sub-menu">
                            <li><a href="#">Bánh cho người mới bắt đầu</a></li>
                            <li><a href="#">Bánh hay làm </a></li>
                            <li><a href="#">Bánh kiểu Âu</a>
                                <ul class="sub-menu">
                                    <li><a href="#">Cách làm bánh kem pho-mát dừa và cà phê</a></li>
                                    <li><a href="#"> Cách làm bánh cheesecake đào </a></li>
                                    <li><a href="#"> Cách làm bánh kem dâu tây</a></li>
                                    <li><a href="#">Cách làm bánh kem dâu tây </a></li>
                                </ul>
                            </li>
                            <li><a href="#">Bánh kiểu Á </a>
                                <ul class="sub-menu">
                                    <li><a href="#">Cách làm bánh kem pho-mát dừa và cà phê</a></li>
                                    <li><a href="#"> Cách làm bánh cheesecake đào </a></li>
                                    <li><a href="#"> Cách làm bánh kem dâu tây</a></li>
                                    <li><a href="#">Cách làm bánh kem dâu tây </a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li><a href="#">Công cụ</a></li>
                    <li><a href="#">Địa chỉ </a></li>
                    <li><a href="#">Liên hệ</a></li>
                </ul>
            </nav>
        </div>
    </body>
</html>