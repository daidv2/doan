<?php 
require 'config/autoload.php';

$controller = filter_input(INPUT_POST, 'controller');
if ($controller== NULL) {
    $controller = filter_input(INPUT_GET, 'controller');
    if ($controller == NULL) {
        $controller = 'CongThucBanh';
    }
}

$action = filter_input(INPUT_POST, 'action');
if ($action== NULL) {
    $action = filter_input(INPUT_GET, 'action');
    if ($action == NULL) {
        $action = 'index';
    }
}

$class = $controller . 'Controller';
$object = new $class();
return $object->$action();

?>